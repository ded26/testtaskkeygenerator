﻿using System;
using KeysGenerator.BLL.DTO;
using KeysGenerator.BLL.Interfaces;
using KeysGenerator.BLL.Services;
using KeysGenerator.DAL.Interfaces;
using KeysGenerator.DAL.Repositories;
using KeysGenerator.PL.Controllers;
using KeysGenerator.PL.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;

namespace KeysGenerator.Tests
{
    [TestClass]
    public class PLTests
    {
        private IKernel _kernel;
        private ICustomerService _service;

        [TestInitialize]
        public void Setup()
        {
            _kernel = new StandardKernel();
            _kernel.Bind<IUnitOfWork>().To<FakeDbUnitOfWork>();
            _kernel.Bind<ICustomerService>().To<CustomerService>();
            _service = _kernel.Get<ICustomerService>();
        }

        [TestMethod]
        public void TestAddView()
        {
            //Arrange
            var homeController = new HomeController();
            //Act
            var result = homeController.CreateCustomer(new CreateCustomerModel { Id = 1, NumberKeys = 4 });
            var result1 = homeController.CreateCustomer(new CreateCustomerModel { Id = 3, NumberKeys = 5});
            //Assert
            Assert.AreEqual(result.Data.ToString(), "Bad");
        }
    }
}
