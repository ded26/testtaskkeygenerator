﻿using System.Linq;
using KeysGenerator.BLL.DTO;
using KeysGenerator.BLL.Infrastructure;
using KeysGenerator.BLL.Interfaces;
using KeysGenerator.BLL.Services;
using KeysGenerator.DAL.Interfaces;
using KeysGenerator.DAL.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;

namespace KeysGenerator.Tests
{
    [TestClass]
    public class BLLTests
    {
        private IKernel _kernel;
        private ICustomerService _service;
        [TestInitialize]
        public void Setup()
        {
            _kernel = new StandardKernel();
            _kernel.Bind<IUnitOfWork>().To<FakeDbUnitOfWork>();
            _kernel.Bind<ICustomerService>().To<CustomerService>();
            _service = _kernel.GetService(typeof(CustomerService)) as ICustomerService;
            
        }

        [TestMethod]
        public void TestValidationCustomerId()
        {
            //Arrange
            _service.CreateCustomer(new CustomerDTO { Id = 1, NumberKeys = 5 });
            _service.CreateCustomer(new CustomerDTO { Id = 2, NumberKeys = 10 });
            //Act
            var result = _service.ValidationCustomerId(new CustomerDTO { Id = 3 });
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestServiceCreateCustomer()
        {
            //Arrange
            _service.CreateCustomer(new CustomerDTO {Id = 1, NumberKeys = 5});
            _service.CreateCustomer(new CustomerDTO {Id = 2, NumberKeys = 10});
            //Act
            var customersCount = _service.GetCustomers().Count();
            
            //Assert
            Assert.AreEqual(2, customersCount);
            
        }

        [TestMethod]
        public void TestServiceGetCustomers()
        {
            //Arrange
            _service.CreateCustomer(new CustomerDTO { Id = 1, NumberKeys = 5 });
            _service.CreateCustomer(new CustomerDTO { Id = 2, NumberKeys = 10 });
            //Act
            var customers = _service.GetCustomers();
            //Assert
            Assert.IsNotNull(customers);
        }

        [TestMethod]
        public void TestServiceGetCustomersKeys()
        {
            //Arrange
            _service.CreateCustomer(new CustomerDTO { Id = 1, NumberKeys = 5 });
            _service.CreateCustomer(new CustomerDTO { Id = 2, NumberKeys = 10 });
            //Act
            var keysFirstCustomer = _service.GetCustomerKey(1);
            var keysFirstCustomerCount = _service.GetCustomerKey(1).Count();
            var keysSecondCustomer = _service.GetCustomerKey(2);
            var keysSecondCustomerCount = _service.GetCustomerKey(2).Count();
            var keysCount = keysFirstCustomerCount + keysSecondCustomerCount;
            //Assert
            Assert.IsNotNull(keysFirstCustomer);
            Assert.AreEqual(5, keysFirstCustomerCount);
            Assert.IsNotNull(keysSecondCustomer);
            Assert.AreEqual(10, keysSecondCustomerCount);
            Assert.AreEqual(15, keysCount);
        }

        [TestMethod]
        [ExpectedException(typeof (ValidationException))]
        public void GetCustomersNull()
        {
            //Act
            var customers = _service.GetCustomers();
        }

       

        [TestMethod]
        [ExpectedException(typeof (ValidationException))]
        public void GetCustomerKeysNull()
        {
            //Act
            var keys = _service.GetCustomerKey(4);
        }
    }
}
