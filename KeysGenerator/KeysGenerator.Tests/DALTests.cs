﻿using System.Collections.Generic;
using KeysGenerator.DAL.Entities;
using KeysGenerator.DAL.Interfaces;
using KeysGenerator.DAL.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;

namespace KeysGenerator.Tests
{
    [TestClass]
    public class DALTests
    {
        private IKernel _ninjectKernel;

        [TestInitialize]
        public void Setup()
        {
            _ninjectKernel = new StandardKernel();
            _ninjectKernel.Bind<IRepository<Customer>>().
                To<CustomersRepository>().
                WithConstructorArgument("customers", new List<Customer>
                {
                    new Customer {Id = 1/*, CustomerKeys = new List<CustomerKey> {new CustomerKey {Id = 1, Value = 1234567890527810, CustomerId = 1} }*/},
                    new Customer {Id = 2/*, CustomerKeys = new List<CustomerKey> {new CustomerKey {Id = 2, Value = 6234527830587819, CustomerId = 2} }*/}
                });
        }

        [TestMethod]
        public void CustomerGetMethodTest()
        {
            //Arrange
            var customersRepository = _ninjectKernel.Get<IRepository<Customer>>();
            //Act
            var notNullCustomer = customersRepository.Get(1);
            var nullCustomer = customersRepository.Get(3);
            //Assert
            Assert.IsNotNull(notNullCustomer);
            Assert.IsNull(nullCustomer);
        }
    }
}
