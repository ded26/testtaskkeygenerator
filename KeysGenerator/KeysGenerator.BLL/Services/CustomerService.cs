﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using KeysGenerator.BLL.BusinessModels;
using KeysGenerator.BLL.DTO;
using KeysGenerator.BLL.Infrastructure;
using KeysGenerator.BLL.Interfaces;
using KeysGenerator.DAL.Entities;
using KeysGenerator.DAL.Interfaces;

namespace KeysGenerator.BLL.Services
{
    public class CustomerService : ICustomerService
    {
        IUnitOfWork FakeCustomerStore { get; }

        public CustomerService(IUnitOfWork fakeCustomerStore)
        {
            FakeCustomerStore = fakeCustomerStore;
        }

        public void CreateCustomer(CustomerDTO customer)
        {
            try
            {
                var keyGenerator = new KeyGenerator(customer.NumberKeys);
                var keys = keyGenerator.GenerateKeys();
                var repositoryKeysCount = FakeCustomerStore.Keys.GetAll().Count();
                var keyId = repositoryKeysCount == 0 ? 0 : FakeCustomerStore.Keys.GetAll().Max(k => k.Id) + 1;
                var newCustomer = new Customer {Id = customer.Id};
                var customerKeys = new List<CustomerKey>();
                foreach (var key in keys)
                {
                    customerKeys.Add(new CustomerKey
                    {
                        Id = keyId,
                        Value = key,
                        CustomerId = newCustomer.Id
                    });
                    keyId++;
                }
                FakeCustomerStore.Customers.Create(newCustomer);
                FakeCustomerStore.Keys.AddRange(customerKeys);
            }
            catch (Exception)
            {
                throw new ValidationException("Error while create customer", "");
            }
        }

        public IEnumerable<CustomerDTO> GetCustomers()
        {
            var customers = FakeCustomerStore.Customers.GetAll().ToList();
            if (customers.Count == 0)
                throw new ValidationException("No customers in repository", "");
            var mapperConfig = new MapperConfiguration(cfg => cfg.CreateMap<Customer, CustomerDTO>());
            var mapper = mapperConfig.CreateMapper();
            return mapper.Map<IEnumerable<Customer>, List<CustomerDTO>>(customers);
        }

        public CustomerDTO GetCustomer(int id)
        {
            var customer = FakeCustomerStore.Customers.Get(id);
            if (customer == null)
                throw new ValidationException("Customer not found", "");
            var mapperConfig = new MapperConfiguration(cfg => cfg.CreateMap<Customer, CustomerDTO>());
            var mapper = mapperConfig.CreateMapper();
            return mapper.Map<Customer, CustomerDTO>(customer);
        }

        public IEnumerable<CustomerKeyDTO> GetCustomerKey(int customerId)
        {
            var customerKeys = FakeCustomerStore.Keys.GetAll().Where(ck => ck.CustomerId == customerId).ToList();
            if (customerKeys.Count == 0)
                throw new ValidationException("This customer has no key", "");
            var mapperConfig = new MapperConfiguration(cfg => cfg.CreateMap<CustomerKey, CustomerKeyDTO>());
            var mapper = mapperConfig.CreateMapper();
            return mapper.Map<IEnumerable<CustomerKey>, List<CustomerKeyDTO>>(customerKeys);
        }

        public bool ValidationCustomerId(CustomerDTO customer)
        {
            var mapperConfig = new MapperConfiguration(cfg => cfg.CreateMap<CustomerDTO, Customer>());
            var mapper = mapperConfig.CreateMapper();
            var dbCustomer = mapper.Map<CustomerDTO, Customer>(customer);
            var result = FakeCustomerStore.Customers.GetAll().FirstOrDefault(c => c.Id == dbCustomer.Id);
            return result == null;
        }
    }
}
