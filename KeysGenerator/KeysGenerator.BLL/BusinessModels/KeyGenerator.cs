﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace KeysGenerator.BLL.BusinessModels
{
    public class KeyGenerator
    {
        private readonly Random _random;
        private readonly int _numberKeys;

        public KeyGenerator(int numberKeys)
        {
            _numberKeys = numberKeys;
            _random = new Random();
        }

        public ICollection<long> GenerateKeys()
        {
            var keys = new List<long>();
            for (var i = 0; i < _numberKeys; i++)
            {
                long key = 0;
                while (key < 1000000000000000)
                {
                    key = key * 10 + _random.Next(0, 9);
                }
                keys.Add(key);
            }
            return keys;
        }



    }
}
