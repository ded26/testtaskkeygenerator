﻿using System.Collections.Generic;
using KeysGenerator.BLL.DTO;

namespace KeysGenerator.BLL.Interfaces
{
    public interface ICustomerService
    {
        void CreateCustomer(CustomerDTO customer);

        IEnumerable<CustomerDTO> GetCustomers();

        CustomerDTO GetCustomer(int id);

        IEnumerable<CustomerKeyDTO> GetCustomerKey(int customerId);

        bool ValidationCustomerId(CustomerDTO customer);
    }
}
