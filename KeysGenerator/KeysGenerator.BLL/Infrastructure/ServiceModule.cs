﻿using KeysGenerator.DAL.Interfaces;
using KeysGenerator.DAL.Repositories;
using Ninject.Modules;

namespace KeysGenerator.BLL.Infrastructure
{
    public class ServiceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUnitOfWork>().To<FakeDbUnitOfWork>();
        }
    }
}
