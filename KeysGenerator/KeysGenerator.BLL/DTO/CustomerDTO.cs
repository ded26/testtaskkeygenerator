﻿namespace KeysGenerator.BLL.DTO
{
    public class CustomerDTO
    {
        public int Id { get; set; }
        public int NumberKeys { get; set; }
    }
}
