﻿namespace KeysGenerator.BLL.DTO
{
    public class CustomerKeyDTO
    {
        public int Id { get; set; }
        public long Value { get; set; }
        public int CustomerId { get; set; }
    }
}
