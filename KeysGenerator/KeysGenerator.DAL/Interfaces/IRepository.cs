﻿using System;
using System.Collections.Generic;

namespace KeysGenerator.DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();

        T Get(int id);

        void Create(T item);

        void Delete(T item);

        void AddRange(IEnumerable<T> collection);

        void ClearReposity();
    }
}
