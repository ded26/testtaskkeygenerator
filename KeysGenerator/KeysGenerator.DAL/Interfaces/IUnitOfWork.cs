﻿using KeysGenerator.DAL.Entities;

namespace KeysGenerator.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<Customer> Customers { get; }
        IRepository<CustomerKey> Keys { get; } 
    }
}
