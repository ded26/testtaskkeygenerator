﻿namespace KeysGenerator.DAL.Entities
{
    public class CustomerKey
    {
        public int Id { get; set; }
        public long Value { get; set; }
        public int CustomerId { get; set; }
        //public Customer Customer { get; set; }
    }
}
