﻿using System.Collections.Generic;
using KeysGenerator.DAL.Entities;
using KeysGenerator.DAL.Interfaces;

namespace KeysGenerator.DAL.Repositories
{
    public class FakeDbUnitOfWork : IUnitOfWork
    {
        private CustomersRepository _customers;
        private KeysRepository _keys;

        public IRepository<Customer> Customers => _customers ?? (_customers = new CustomersRepository(new List<Customer>()));
        public IRepository<CustomerKey> Keys => _keys ?? (_keys = new KeysRepository(new List<CustomerKey>()));
    }
}
