﻿using System.Collections.Generic;
using System.Linq;
using KeysGenerator.DAL.Entities;
using KeysGenerator.DAL.Interfaces;

namespace KeysGenerator.DAL.Repositories
{
    public class CustomersRepository : IRepository<Customer>
    {
        private readonly List<Customer> _customers;
        public CustomersRepository(IEnumerable<Customer> customers)
        {
            _customers = customers.ToList();
        }

        public IEnumerable<Customer> GetAll()
        {
            return _customers;
        }

        public Customer Get(int id)
        {
            return _customers.FirstOrDefault(c => c.Id == id);
        }

        public void Create(Customer customer)
        {
            _customers.Add(customer);
        }

        public void Delete(Customer customer)
        {
            _customers.Remove(customer);
        }

        public void AddRange(IEnumerable<Customer> collection)
        {
            _customers.AddRange(collection);
        }

        public void ClearReposity()
        {
            _customers.Clear();
        }
    }
}
