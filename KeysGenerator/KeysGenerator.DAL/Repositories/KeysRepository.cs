﻿using System.Collections.Generic;
using System.Linq;
using KeysGenerator.DAL.Entities;
using KeysGenerator.DAL.Interfaces;

namespace KeysGenerator.DAL.Repositories
{
    public class KeysRepository : IRepository<CustomerKey>
    {
        private readonly List<CustomerKey> _keys;

        public KeysRepository(IEnumerable<CustomerKey> keys)
        {
            _keys = keys.ToList();
        }

        public IEnumerable<CustomerKey> GetAll()
        {
            return _keys;
        }

        public CustomerKey Get(int id)
        {
            return _keys.FirstOrDefault(c => c.Id == id);
        }

        public void Create(CustomerKey item)
        {
            _keys.Add(item);
        }

        public void Delete(CustomerKey item)
        {
            _keys.Remove(item);
        }

        public void AddRange(IEnumerable<CustomerKey> collection)
        {
            _keys.AddRange(collection);
        }

        public void ClearReposity()
        {
            _keys.Clear();
        }
    }
}
