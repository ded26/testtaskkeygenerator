﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KeysGenerator.BLL.Infrastructure;
using Microsoft.Ajax.Utilities;

namespace KeysGenerator.PL.Filters
{
    public class ValidationError : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            var exception = filterContext.Exception as ValidationException;
            if (exception == null) return;
            filterContext.ExceptionHandled = true;
            filterContext.Result = new JsonResult
            {
                Data = new {success = false, validationError = exception.Message},
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}