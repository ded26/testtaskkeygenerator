﻿namespace KeysGenerator.PL.Models
{
    public class CreateCustomerModel
    {
        public int Id { get; set; }
        public int NumberKeys { get; set; }
    }
}