﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using KeysGenerator.BLL.DTO;
using KeysGenerator.BLL.Infrastructure;
using KeysGenerator.BLL.Interfaces;
using KeysGenerator.PL.Filters;
using KeysGenerator.PL.Models;

namespace KeysGenerator.PL.Controllers
{
    public class HomeController : Controller
    {
        private ICustomerService Service { get; }

        public HomeController()
        {
            Service = DependencyResolver.Current.GetService<ICustomerService>();
        }

        // GET: Home
        public ActionResult Index() => View();

        [HttpPost]
        public JsonResult CreateCustomer(CreateCustomerModel customer)
        {

            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CreateCustomerModel, CustomerDTO>();
                cfg.CreateMap<CustomerDTO, ViewCustomerModel>();
            });
            var mapper = mapperConfig.CreateMapper();
            var customerDto = mapper.Map<CreateCustomerModel, CustomerDTO>(customer);
            if (!Service.ValidationCustomerId(customerDto))
                return Json(new {success = false, message = "Id is already exists"}, JsonRequestBehavior.AllowGet);
            Service.CreateCustomer(customerDto);
            var jsonData = new
            {
                success = true,
                data = mapper.Map<IEnumerable<CustomerDTO>, List<ViewCustomerModel>>(Service.GetCustomers())
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [ValidationError]
        public JsonResult GetCustomers()
        {
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CustomerDTO, ViewCustomerModel>();
            });
            var mapper = mapperConfig.CreateMapper();
            var jsonData =
                new
                {
                    success = true,
                    data = mapper.Map<IEnumerable<CustomerDTO>, List<ViewCustomerModel>>(Service.GetCustomers())
                };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCustomerKey(int customerId)
        {
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CustomerKeyDTO, ViewCustomerKeyModel>();
            });
            var mapper = mapperConfig.CreateMapper();
            var jsonData = new
            {
                success = true,
                data =
                    mapper.Map<IEnumerable<CustomerKeyDTO>, List<ViewCustomerKeyModel>>(
                        Service.GetCustomerKey(customerId))
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
    }
}