﻿function successFunc(serverData) {
    if (serverData.success) {
        var results = $("#tabs-2");
        results.empty();
        results.append("<ul class=\"customers\"></ul>");
        $.each(serverData.data, function() {
            results.append("<li class=\"cs\">" +
                "<a href=/Home/GetCustomerKey?customerId=" + this.Id + ">" + this.Id + "</a></li>");
        });
        results.append("</ul>");
        $("#results").text("The customer was created successfully");
        $("#results").css({
            "display": "block"
        });
        $("#ValidationId").text("");
        $("#ValidationId").css({
            "display": "none"
        });
        $.getScript("/scripts/CustomScripts/CustomerClick.js");
    } else {
        $("#results").text("");
        $("#results").css({
            "display": "none"
        });
        $("#ValidationId").text(serverData.message);
        $("#ValidationId").css({
            "display": "block"
        });
    }
};