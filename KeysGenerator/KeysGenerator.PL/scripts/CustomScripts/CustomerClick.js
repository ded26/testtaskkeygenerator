﻿$(document).ready(function() {
    $(".cs a").click(function (e) {
        e.preventDefault();
        var url = $(this).attr("href");
        var customer = $(this).parent();
        if (!$(customer).has("ol").length) {
            $.getJSON(url, null, function(keys) {
                if (keys.success) {
                    var list = $("<ol/>");
                    $(keys.data).each(function(index, value) {
                        $(list).append("<li>" + value.Value + "</li>");
                    });
                    $(customer).append(list);
                }
            });
        } else {
            $(customer).find("ol").remove();
        }
    });
});
