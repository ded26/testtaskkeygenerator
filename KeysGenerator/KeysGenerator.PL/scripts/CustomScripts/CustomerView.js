﻿$(document).ready(function() {
    var results = $("#tabs-2");
    results.empty();
    $.getJSON("/Home/GetCustomers", null, function (serverData) {
        if (serverData.success) {
            var list = results.append("<ul class=\"customers\"></ul>").find("ul");
            $.each(serverData.data, function() {
                list.append("<li class=\"cs\"><a href=/Home/GetCustomerKey?customerId=" + this.Id + ">" + this.Id + "</a></li>");
            });
            $.getScript("/scripts/CustomScripts/CustomerClick.js");
        } else {
            results.append("<h3>" + serverData.validationError + "</h3>");
        }
    });
    var customer = $("#ValidationId");
    customer.css("display", "none");
});

