﻿using System.Web.Optimization;

namespace KeysGenerator.PL.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/scripts").Include("~/scripts/*.js"));
            bundles.Add(new ScriptBundle("~/customerView").Include("~/scripts/CustomScripts/CustomerView.js",
                                                                   "~/scripts/CustomScripts/CreateCustomerOnSuccess.js"));
            bundles.Add(new StyleBundle("~/styles").Include("~/Content/themes/base/*.css"));
        }
    }
}