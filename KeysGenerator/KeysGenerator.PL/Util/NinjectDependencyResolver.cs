﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using KeysGenerator.BLL.Interfaces;
using KeysGenerator.BLL.Services;
using Ninject;
using Ninject.Web.Common;

namespace KeysGenerator.PL.Util
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private readonly IKernel _kernel;

        public NinjectDependencyResolver(IKernel kernel)
        {
            _kernel = kernel;
            AddBindings();
        }

        private void AddBindings()
        {
            _kernel.Bind<ICustomerService>().To<CustomerService>().InSingletonScope();
        }

        public object GetService(Type serviceType)
        {
            return _kernel.GetService(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }
    }
}